<img src="https://raw.githubusercontent.com/kelaberetiv/TagUI/master/src/media/tagui_logo.png" height="111" align="right">

# TagUI
>**NOTICE: The AISG team is discontinuing the maintenance and support of TagUI. As of Q4 2023, we will no longer provide updates or support. Meanwhile, do expect delayed response from our team. It is easy to migrate TagUI .tag workflow files to Python rpa package .py scripts. [See this link](https://github.com/tebelorg/RPA-Python/issues/484).**

**To start, click the download link below. Take the [free course](https://learn.aisingapore.org/courses/learn-rpa-with-tagui-beginners-course/). Ask any questions at [our Telegram](https://t.me/rpa_chat).**

**[Download v6.114](https://tagui.readthedocs.io/en/latest/setup.html)&ensp;|&ensp;[Usage Guide](https://tagui.readthedocs.io/en/latest/index.html)&ensp;|&ensp;[Demos](https://github.com/aimakerspace/TagUI-Bricks)&ensp;|&ensp;[Samples](https://github.com/kelaberetiv/TagUI/tree/master/flows/samples)&ensp;|&ensp;[Slides](https://docs.google.com/presentation/d/1pltAMzr0MZsttgg1w2ORH3ontR6Q51W9/edit?usp=sharing&ouid=115132044557947023533&rtpof=true&sd=true)&ensp;|&ensp;[Podcast](https://botnirvana.org/podcast/tagui/)&ensp;|&ensp;[Video](https://www.youtube.com/watch?v=C5itbB3sCq0)&ensp;|&ensp;[中文](http://www.tagui.com.cn)**

---

# Run options
You can use the below options when running tagui.

For example, the command below runs my_flow.tag without showing the web browser, while storing the flow run result in tagui_report.csv.

```
tagui my_flow.tag -headless -report
```

-deploy or -d
Deploys a flow, creating a shortcut which can be double-clicked to run the flow. If the flow file is moved, a new shortcut must be created. The flow will be run with all the options used when creating the shortcut.

-headless or -h
Runs the flow with an invisible Chrome web browser (does not work for visual automation).

-nobrowser or -n
Runs without any web browser, for example to perform automation only with visual automation.

-report or -r
Tracks flow run result in ```tagui/src/tagui_report.csv``` and saves html logs of flows execution.

-turbo or -t
Run automation at 10X the speed of normal human user. Read caveats at Advanced concepts!

-quiet or -q
Runs without output to command prompt except for explicit output (echo, show, check steps and errors etc). To have fine-grained control on showing and hiding output during execution (eg hiding password from showing up), use ```quiet_mode = true``` and ```quiet_mode = false``` in your flow.

-edge or -e
Runs using Microsoft Edge browser instead of Chrome (can be used with -headless option).

my_datatable.csv
Uses the specified csv file as the datatable for batch automation. See datatables.

input(s)
Add your own parameter(s) to be used in your automation flow as variables p1 to p8.

For example, from the command prompt, below line runs register_attendence.tag workflow using Microsoft Edge browser and with various student names as inputs.

```
tagui register_attendence.tag -edge Jenny Jason John Joanne
```
Inside the workflow, the variables p1, p2, p3, p4 will be available for use as part of the automation, for example to fill up student names into a web form for recording attendence. The following lines in the workflow will output various student names given as inputs.

```
echo `p1`
echo `p2`
echo `p3`
echo `p4`
```